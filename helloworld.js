var sHello = ""
for (var i = 0; i < 5; i++)
{
    sHello += "a"
}
document.write("Hello World!" + sHello + "<br>");

var sOutput = "========StateMachine========<br>";
var sStates = ["A", "B", "C"];
var fsm = new StateMachine();
sStates.forEach(function(sState, i){
    fsm.AddState(sState,
        function(){ sOutput += "Enter " + sState + "<br>"; },
        function(){ sOutput += "Exit " + sState + "<br>"; }
    );
});
for (i = 0; i < sStates.length; ++i)
{
    var i2 = i + 1;
    if (i2 == sStates.length) i2 = 0;
    fsm.AddTransfer(sStates[i], String(i), sStates[i2]);
}
if (fsm.SetState(sStates[0], true))
    sOutput += "Init OK, Current State : " + fsm.GetCurrentState() + "<br>";
if (fsm.SetState(sStates[1]))
    sOutput += "Set OK, Current State : " + fsm.GetCurrentState() + "<br>";
if (!fsm.SetState(sStates[0]))
    sOutput += "Set Fail, Current State : " + fsm.GetCurrentState() + "<br>";
if (fsm.TriggerEvent("1"))
    sOutput += "Transfer OK, Current State : " + fsm.GetCurrentState() + "<br>";
if (!fsm.TriggerEvent("1"))
    sOutput += "Transfer Fail, Current State : " + fsm.GetCurrentState() + "<br>";

sOutput += "========IDPool========<br>"
var idPool = new IDPool(0, 10, 3);
idPool.Declare(3);
idPool.Declare(5);
idPool.Declare(9);
for (var i = 0; i < 5; ++i)
{
    if (i !== 0)
        sOutput += ", ";
    sOutput += idPool.Generate();
}
idPool.Free(1);
idPool.Free(2);
var nGen = idPool.Generate();
while (nGen !== 3)
{
    sOutput += ", " + nGen;
    nGen = idPool.Generate();
}
sOutput += "<br>";

sOutput += "========MD5========<br>"
var md5obj = md5.create();
md5obj.update("sb");
sOutput += md5obj.hex() + "<br>";

sOutput += "========UTF8========<br>"
sOutput += utf8.encode("啊") + "<br>";

sOutput += "========format========<br>"
sOutput += format("Num: %d/%s", 10, "8") + "<br>";

sOutput += "========Random========<br>"
sOutput += "Random : " + Random.RandInt(-10, 10) + ", " +
    Random.RandFloat(0.1, 0.5) + ", " + Random.RandFloat(-10, 10) + "<br>";
var normLots = [];
for (var i = 0; i < 10; ++i)
    normLots.push(i);
sOutput += "DrawLots : " + Random.DrawLots(normLots);
var weightLots = [
    [0, -1], [1, 0.5], [2, 0.5], [3, 0.1]
];
sOutput += ", " + Random.DrawLotsW(weightLots);
var results = [];
Random.DrawLotsM(normLots, 3, results);
sOutput += ", ";
results.forEach(function(nValue, i) { sOutput += " " + nValue; });
results = [];
Random.DrawLotsWM(weightLots, 2, results);
sOutput += ", ";
results.forEach(function(nValue, i) { sOutput += " " + nValue; });
sOutput += "<br>"

document.write(sOutput);